![Screenshot preview of the theme "BIOS" by glenthemes](https://64.media.tumblr.com/818f7a3faabfbe0bb9c066fde5674557/121b20257817921b-fe/s1280x1920/ff254e7c8b58cd442b7dae4397ea1c07a9b77d53.png)

**Theme no.:** 30  
**Theme name:** BIOS  
**Theme type:** Free / Tumblr use  
**Description:** A moderately minimal theme inspired by clean sci-fi UI, starring 2B from NieR: Automata.  
**Author:** @&hairsp;glenthemes  

**Release date:** [2018-06-15](https://64.media.tumblr.com/e30e0acf791bd83142ef93861b9168f2/tumblr_padc3mkzFh1ubolzro1_1280.png)  
**Rework date:** 2022-02-21

**Post:** [glenthemes.tumblr.com/post/174915069179](https://glenthemes.tumblr.com/post/174915069179)  
**Preview:** [glenthpvs.tumblr.com/bios](https://glenthpvs.tumblr.com/bios)  
**Download:** [pastebin.com/DbGjvVtp](https://pastebin.com/DbGjvVtp)  
**Credits:** [glencredits.tumblr.com/bios](https://glencredits.tumblr.com/bios)
