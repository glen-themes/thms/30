var root = getComputedStyle(document.documentElement);

var customize_page = window.location.href.indexOf("/customize") > -1;
var on_main = window.location.href.indexOf("/customize") < 0;

document.addEventListener("DOMContentLoaded",() => {
    var bob = document.getElementsByTagName("html")[0];

    if(customize_page){
        bob.setAttribute("customize-page","true");
    } else if(on_main){
        bob.setAttribute("customize-page","false");
    }
    
});

var floopie = new Image();
floopie.src = "https://cdn.discordapp.com/attachments/382037367940448256/944886339239825438/unknown.png";
    
// audio post play button - flaticon.com/free-icon/play-button_152770
var playb = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' version='1.1' id='Layer_1' x='0px' y='0px' viewBox='0 0 330 330' style='enable-background:new 0 0 330 330;' xml:space='preserve'> <path id='XMLID_497_' d='M292.95,152.281L52.95,2.28c-4.625-2.891-10.453-3.043-15.222-0.4C32.959,4.524,30,9.547,30,15v300 c0,5.453,2.959,10.476,7.728,13.12c2.266,1.256,4.77,1.88,7.272,1.88c2.763,0,5.522-0.763,7.95-2.28l240-149.999 c4.386-2.741,7.05-7.548,7.05-12.72C300,159.829,297.336,155.022,292.95,152.281z M60,287.936V42.064l196.698,122.937L60,287.936z'/> </svg>";

document.documentElement.style.setProperty('--audioplay','url("' + playb + '")');

// audio post pause button - flaticon.com/free-icon/pause_747384
var pauseb = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' version='1.1' id='Capa_1' x='0px' y='0px' viewBox='0 0 512 512' style='enable-background:new 0 0 512 512;' xml:space='preserve'> <g> <g> <path d='M154,0H70C42.43,0,20,22.43,20,50v412c0,27.57,22.43,50,50,50h84c27.57,0,50-22.43,50-50V50C204,22.43,181.57,0,154,0z M164,462c0,5.514-4.486,10-10,10H70c-5.514,0-10-4.486-10-10V50c0-5.514,4.486-10,10-10h84c5.514,0,10,4.486,10,10V462z'/> </g> </g> <g> <g> <path d='M442,0h-84c-27.57,0-50,22.43-50,50v412c0,27.57,22.43,50,50,50h84c27.57,0,50-22.43,50-50V50C492,22.43,469.57,0,442,0z M452,462c0,5.514-4.486,10-10,10h-84c-5.514,0-10-4.486-10-10V50c0-5.514,4.486-10,10-10h84c5.514,0,10,4.486,10,10V462z'/> </g> </g> </svg>";

document.documentElement.style.setProperty('--audiopause','url("' + pauseb + '")');

// audio post 'install audio' button
// feathericons
var cdrii = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='black' stroke-width='1.5' stroke-linecap='round' stroke-linejoin='round' class='feather feather-hash'><path d='M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4'></path><polyline points='7 10 12 15 17 10'></polyline><line x1='12' y1='15' x2='12' y2='3'></line></svg>";

document.documentElement.style.setProperty('--install','url("' + cdrii + '")');

// external link icon
// feathericons
var schtd = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='1.5' stroke-linecap='round' stroke-linejoin='round' class='feather feather-external-link'><path d='M18 13v6a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h6'/><polyline points='15 3 21 3 21 9'/><line x1='10' y1='14' x2='21' y2='3'/></svg>";

document.documentElement.style.setProperty('--ext','url("' + schtd + '")');

// =>

$(document).ready(function(){
    // check jquery version
    var jqver = jQuery.fn.jquery;
    jqver = jqver.replaceAll(".","");
    
    $(".tumblr_preview_marker___").remove();
    
    /*------- NPF VIDEO STUFF -------*/
    $(".photo-origin").each(function(){
        if($(this).find("[data-npf*='video']").length){
            $(this).addClass("npf-video")
        }
    })
    
    $("video[autoplay='autoplay']").each(function(){
        $(this).removeAttr("autoplay")
    });
    
    /*----------- REMOVE <p> WHITESPACE -----------*/
    $(".postinner p, .postinner blockquote").each(function(){
        if(!$(this).prev().length){
            if($(this).parent().is(".postinner")){

                $(this).css("margin-top",0)
            }
        }
        
        if(!$(this).next().length){
            // target last <p>
            // if it's empty, remove
            if($.trim($(this).html()) == ""){
                $(this).remove();
            }
        }
    })
    
    $(".postinner p, .postinner blockquote, .postinner ol, .postinner ul").each(function(){
        if(!$(this).next().length){
            // target last <p>
            // if no next sibling, negate bottom padding
            $(this).css("margin-bottom",0)
        }
        
        if($(this).next().is(".tagsdiv")){
            $(this).css("margin-bottom",0)
        }
    })
    
    // remove empty captions
    $(".caption").each(function(){
        if($.trim($(this).text()) == ""){
            $(this).remove()
        }
    })
    
    if(customize_page){
        $(".reblog-comment > p[last-comment]:first-child").each(function(){
            $(this).css("margin-top",0)
        })
    }
    
    $("[original-post] .postinner").each(function(){
        $(this).contents().filter(function(){
            return this.nodeType == 3 && this.data.trim().length > 0
        }).wrap("<span/>")
    })
    
    $("[reblogged-post] .reblog-comment").each(function(){
        $(this).contents().filter(function(){
            return this.nodeType == 3 && this.data.trim().length > 0
        }).wrap("<span/>")
    })
    
    $(".posts").each(function(){
        $(this).find("p, h1, h2, h3, h4, h5, h6, blockquote, ol, ul, pre").each(function(){
            if(!$(this).next().length){
                $(this).css("margin-bottom",0)
            }
        })
    })
    
    /*----------- REBLOG-HEAD -----------*/
    $(".reblog-url").each(function(){
        var uz = $.trim($(this).text());
        if(uz.indexOf("-deac") > 0){
            var rogner = uz.substring(0,uz.lastIndexOf("-"));
            $(this).find("a").attr("href","//" + rogner + ".tumblr.com");
            $(this).find("a").text(rogner);
            $(this).append("<span class='deac'>(deactivated)</span>")
        }
    })
    
    /*-------- AUDIO BULLSH*T --------*/
    var mtn = Date.now();
    var fvckme = setInterval(function(){
        if(Date.now() - mtn > 1000){
            clearInterval(fvckme);
            $(".audiowrap").each(function(){
                $(this).prepend("<audio src='" + $(this).attr("audio-src") + "'>");
            });
            
            $(".inari").each(function(){
                var m_m = $(this).parents(".audiowrap").attr("audio-src");
                $(this).attr("href",m_m);
            })
        } else {
            $(".tumblr_audio_player").each(function(){
                if($(this).is("[src]")){
                    var audsrc = $(this).attr("src");
                    audsrc = audsrc.split("audio_file=").pop();
                    audsrc = decodeURIComponent(audsrc);
                    audsrc = audsrc.split("&")[0];
                    $(this).parents(".audiowrap").attr("audio-src",audsrc)
                }
            })
        }
    },0);
    
    $(".albumwrap").click(function(){
        
        var emp = $(this).parents(".audiowrap").find("audio")[0];
        
        if(emp.paused){
            emp.play();
            $(".overplay",this).addClass("ov-z");
            $(".overpause",this).addClass("ov-y");
        } else {
            emp.pause();
            $(".overplay",this).removeClass("ov-z");
            $(".overpause",this).removeClass("ov-y");
        }
        
        var that = this
        
        emp.onended = function(){
            $(".overplay",that).removeClass("ov-z");
            $(".overpause",that).removeClass("ov-y");
        }
    })
    
    // minimal soundcloud player @ shythemes.tumblr
    var soucolor = $("html[pmbg]").attr("pmbg");
    
    $('iframe[src*="soundcloud.com"]').each(function(){
        $(this).one("load",function(){
            soundfvk()
        });
    });
    
    function soundfvk(){
       $('iframe[src*="soundcloud.com"]').each(function(){
           $(this).attr({ src: $(this).attr('src').split('&')[0] + '&amp;liking=false&amp;sharing=false&amp;auto_play=false&amp;show_comments=false&amp;continuous_play=false&amp;buying=false&amp;show_playcount=false&amp;show_artwork=true&amp;origin=tumblr&amp;color=' + soucolor.split('#')[1], height: 116, width: '100%' });
       });
    }
    
    $(".soundcloud_audio_player").each(function(){
        $(this).wrap("<div class='audio-soundcloud'>")
    })
    
    /*-------- QUOTE SOURCE BS --------*/
    $(".quote-source").each(function(){
        $(this).contents().filter(function(){
            return this.nodeType == 3 && this.data.trim().length > 0
        }).wrap("<span>");
        $(this).find("a.tumblr_blog").remove();
        
        $(this).find("span").each(function(){
            if($.trim($(this).text()) == "(via"){
                $(this).remove();
            }
            
            if($.trim($(this).text()) == ")"){
                $(this).remove();
            }
        })
        
        $(this).html(
            $(this).html().replace("(via","")
        )
        
        $(this).find("span:first").each(function(){
            if(!$(this).next().length){
                $(this).remove()
            }
        })
        
        $(this).find("p").each(function(){
            if($.trim($(this).text()) == ""){
                $(this).remove();
            }
            
            if($(this).children("br").length){
                if(!$(this).children().first().siblings().length){
                    $(this).remove()
                }
            }
            
            $(this).html($.trim($(this).html()));
            
            if($(this).text() == ")"){
                $(this).remove()
            }
        })
        
        $(this).find("p:last").each(function(){
            if($(this).find("a[href*='tumblr.com/post']").length){
                $(this).remove()
            }
        })
        
        $(this).find("p:last").each(function(){
            if(!$(this).next().length){
                $(this).css("margin-bottom",0)
            }
        })
    })
    
    $("[mdash] + p").each(function(){
        if(!$(this).next().length){
            if($.trim($(this).text()) !== ""){
                var sto = " " + $(this).html();
                $(this).prev().append(sto)
                $(this).remove();
            }
        }
    })
    
    /*-------- ASK/ANSWER POSTS --------*/
    $(".question_text").each(function(){
        if(!$(this).children().first().is("p")){
            $(this).wrapInner("<p></p>")
        }
    })
    
    /*-------- CHAT POSTS --------*/
    $(".npf_chat").each(function(){
        $(this).find("b").each(function(){
            var cb = $(this).html();
            $(this).before("<div class='chat_label'>" + cb + "</div>");
            $(this).remove()
        })
        
        $(this).contents().filter(function(){
            return this.nodeType == 3 && this.data.trim().length > 0
        }).wrap("<div class='chat_content'>");
        
        $(this).wrap("<div class='chat_row'>");
        $(this).children().unwrap()
    })
    
    $(".chat_row").each(function(){
        $(this).not(".chat_row + .chat_row").each(function(){
            if(jqver < "180"){
                $(this).nextUntil(":not(.chat_row)").andSelf().wrapAll('<div class="chatwrap">');
            } else {
                $(this).nextUntil(":not(.chat_row)").addBack().wrapAll('<div class="chatwrap">');
            }
        });
    })
    
    /*---- MAKE SURE <p> IS FIRST CHILD OF RB ----*/
    $(".reblog-head").each(function(){
        if(!$(this).next(".reblog-comment").length){
            $(this).nextUntil(".tagsdiv").wrapAll("<div class='reblog-comment'>")
        }
    })
    
    $(".reblog-comment").each(function(){
        if($(this).children().first().is("div")){
            $(this).prepend("<p></p>")
        }
    })
    
    
    
    /*-------- CLICKTAGS --------*/
    var tags_ms = parseInt(getComputedStyle(document.documentElement)
                   .getPropertyValue("--Tags-Fade-Speed-MS"));
    
    
    $(".clicktags").click(function(){
        var that = this;
        var tagsdiv = $(this).parents(".permadiv").prev(".postinner").find(".tagsdiv");
        
        if(!$(this).hasClass("clique")){
            $(this).addClass("clique");
            tagsdiv.slideDown(tags_ms);
            setTimeout(function(){
                tagsdiv.addClass("tagsfade");
            },tags_ms);
        } else {
            tagsdiv.removeClass("tagsfade");
            setTimeout(function(){
                tagsdiv.slideUp(tags_ms);
                $(that).removeClass("clique");
            },tags_ms)
        }
    })
    
    // remove tumblr redirects script by magnusthemes@tumblr
    // part 1/2
    $('a[href*="t.umblr.com/redirect"]').each(function(){
      var originalURL = $(this).attr("href").split("?z=")[1].split("&t=")[0];
      var replaceURL = decodeURIComponent(originalURL);
      $(this).attr("href", replaceURL);
    });
    
    // part 2/2
    function noHrefLi(){
        var linkSet = document.querySelectorAll('a[href*="href.li/?"]');
        Array.prototype.forEach.call(linkSet,function(el,i){
            var theLink = linkSet[i].getAttribute('href').split("href.li/?")[1];
            linkSet[i].setAttribute("href",theLink);
        });
    }
    noHrefLi();
    
    // make iframe heights look more 'normal'
	$(".embed_iframe").each(function(){
        if($(this).parent().is(".tmblr-embed")){
            var wut = $(this).width();
            
            var wrath = $(this).attr("width");
            var rat_w = wrath / wut;
            
            var hrath = $(this).attr("height");
            var rat_h = hrath / rat_w;
            
            $(this).height(rat_h)
        }
    })
    
    /*----- OTHER -----*/
    if(customize_page){
        $(".reblog-head img").each(function(){
            if($(this).attr("src") == $("html[portrait]").attr("portrait")){
                $(this).remove()
            }
        })
    }
    
    $(".chat_content").each(function(){
        if($.trim($(this).text()).indexOf("{block:") > -1){
            var notgod = $(this).html();
            notgod.replaceAll("{","&lcub;").replaceAll("}","&rcub;");
            $(this).before("<code>" + notgod + "</code>");
            $(this).remove()
        }
    })
    
    if(!$(".navz a[href*='glenthemes']").length){
        $("body").eq(0).append("<a class=\'yeoup pz\' title=\'&#x226A;&#x2009;BIOS&#x2009;&#x226B; by glenthemes\' href=\'//glenthemes.tumblr.com\' style=\'display:block!important\'>theme</a>");
        $(".yeoup").each(function(){
            if(!$(this).hasClass("pz")){
                $(this).remove();
            }
        })
    }
    
    // remove extra spacing on the last post if there's nothing after it
    $(".posts:last-child").each(function(){
        if(!$(this).next().length){
            $(this).css("margin-bottom",0)
        }
    })
    
    // if url is /ask and user doesn't have /ask enabled:
    var yslg = window.location.href;
    yslg = yslg.lastIndexOf('/') == yslg.length - 1 ? yslg.substr(0, yslg.length - 1) : yslg.substr(0, yslg.length + 1);
    var tgASK = yslg.substr(yslg.lastIndexOf('/') + 1);
    var bgnm = $("html[blog-name]").attr("blog-name");
    
    if(tgASK == "ask"){
        $(".posts:only-child").each(function(){
            $(this).find(".post-title").eq(0).each(function(){
                if($(this).text().toLowerCase() == "not found"){
                    if($(this).next("p").text().toLowerCase() == "the url you requested could not be found."){
                        $(this).next("p").html(
                            "@" + bgnm.toUpperCase() + " has not enabled their askbox.<p>To enable the askbox, follow these steps:<p style='margin-bottom:0px;'><img src='https://cdn.discordapp.com/attachments/382037367940448256/944886339239825438/unknown.png'>"
                        )
                    }
                }
            })
        })
    }
    
    /*----- REMOVE [,] FROM NOTE COUNT -----*/
    var cmnz = $.trim(root.getPropertyValue("--Remove-Comma-From-NoteCount"));
    
    if(cmnz == "yes"){
        $(".permadiv [nc], .notescont h1:first").each(function(){
            var ekgjei = $.trim($(this).html());
            ekgjei = ekgjei.replaceAll(",","");
            $(this).html(ekgjei)
        })
    }
    
    /*--- REMOVE "DEACTIVATED" FROM USER URL ---*/
    $("[rb-root-name]").each(function(){
        var exizR = $(this).attr("title");
        var rootname = $(this).attr("rb-root-name");
        var rootSUB = rootname.substr(rootname.lastIndexOf("-")+1);
    	
    	if(rootSUB.slice(0,4) == "deac"){
    		var rmDEAC = rootname.substring(0,rootname.lastIndexOf("-"));
    		$(this).attr("title",exizR + rmDEAC + " (deactivated)")
    	} else {
    	    $(this).attr("title",exizR + rootname)
    	}
    })
    
    /*--- SIDEBAR 1 STUFF ---*/
    var SB1W = $.trim(root.getPropertyValue("--Sidebar-1-Width"));
    if(/^\d+$/.test(SB1W)){
        document.documentElement.style.setProperty("--Sidebar-1-Width", SB1W + "px");
    }
    $(".desc").contents().filter(function(){
        return this.nodeType == 3 && this.data.trim().length > 0
    }).wrap("<span d/>");
    
    // if user wants to have a chonky first letter:
    if($(".desc").length){
        $(".desc[flz='true'] [flc]").each(function(){
            var onst = $.trim(root.getPropertyValue("--Desc-First-Letter"));
            onst = onst.slice(0,1);
            
            $(this).prepend("<span fl>" + onst + "</span>");
            
            $(".desc span[d]").eq(0).each(function(){
                // $(this).html("<span d-inv>" + onst + "</span>" + $(this).html())
                $(this).before("<span d-inv>" + onst + "</span>")
            })
        })
    }
    
    // if user watns chonky first letter, and
    // first character of the *actual desc* is a visible space
    $(".desc [d-inv] + span[d]").each(function(){
        var v_start = $(this).html().slice(0,1);
        var v_end = $(this).html().slice(-1);
        if(/\s/.test(v_end)){
            $(this).html($(this).html().trim() + " ")
        }
    })
    
    $(".desc span[d]").contents().unwrap();
    
    /*--- SIDEBAR 2 STUFF ---*/
    var CEGI = $.trim(root.getPropertyValue("--Center-Image-Width"));
    if(/^\d+$/.test(CEGI)){
        document.documentElement.style.setProperty("--Center-Image-Width", CEGI + "px");
    }
    
    $(".center-img img").each(function(){
        $(this).wrap("<div class='dvinviz'></div>");
    })
    
    /*--- COLUMN GAPS ---*/
    var MCOLGAP1 = $.trim(root.getPropertyValue("--Column-Gap-1"));
    if(/^\d+$/.test(MCOLGAP1)){
        document.documentElement.style.setProperty("--Column-Gap-1", MCOLGAP1 + "px");
    }
    
    var MCOLGAP2 = $.trim(root.getPropertyValue("--Column-Gap-2"));
    if(/^\d+$/.test(MCOLGAP2)){
        document.documentElement.style.setProperty("--Column-Gap-2", MCOLGAP2 + "px");
    }
    
    /*--- MORE POST STUFF ---*/
    var POSTW = $.trim(root.getPropertyValue("--Post-Width"));
    if(/^\d+$/.test(POSTW)){
        document.documentElement.style.setProperty("--Post-Width", POSTW + "px");
    }
    
    /*--- READ MORES ---*/
    $("p:has(a.read_more)").each(function(){
        $(this).addClass("p_read_more")
    })
    
    /*--- CUSTOMIZE PANEL MESSAGE ---*/
    if(customize_page){
        if($("html").is("[cmsg='show']")){
            $("body").eq(0).prepend("<div class='eugta'>Please read <a href=\'//docs.google.com/presentation/d/1zh33WY1-EcWa9PcA_rDN7Pwk5YvlOTBQYEdKGBeFc0E/edit?usp=sharing\'>the guide</a> to get started.</div>")
        }
    }
    
    /*-------- TOOLTIPS --------*/
    $("[title]").each(function(){
        if($.trim($(this).attr("title")) !== ""){
            $(this).addClass("a-has-title")
        }
        
        if($(this).is("a")){
            if($(this).is("[href]")){
                if($.trim($(this).attr("href")) == ""){
                    $(this).css("cursor","help")
                }
            } else {
                $(this).css("cursor","help")
            }
        }
    })
    
    $(".a-has-title").style_my_tooltips({
        tip_follows_cursor:true,
        tip_delay_time:0,
        tip_fade_speed:0,
        attribute:"title"
    });
    
});//end jquery / end ready
